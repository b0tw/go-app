const Sequelize = require('sequelize');

const sequelize = new Sequelize('go-app', 'root', '', {
    host: 'localhost',
    dialect: 'mysql'
});

module.exports = sequelize;