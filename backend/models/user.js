const DataTypes = require('sequelize');
const sequelize = require('../config').db;

const User = sequelize.define('User', {
    firstName: {
        type: DataTypes.STRING
    },
    lastName: {
        type: DataTypes.STRING
    },
    email: {
        type: DataTypes.STRING
    },
    password: {
        type: DataTypes.STRING
    }
}, { updatedAt: false });

module.exports = User;
