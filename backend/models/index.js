const db = require('../config').db;

const User = require('./user');

module.exports = {
    User,
    connection: db
};