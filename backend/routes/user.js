const express = require('express');
const user = require('../controllers').User;
const auth = require('../controllers').Auth;

const router = express.Router();

router.post('/register', user.register);
router.post('/update', auth.isAuthenticated, user.update);

module.exports = router;