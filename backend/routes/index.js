const express = require('express');
const otherRouter = require('./other');
const authRouter = require('./auth');
const userRouter = require('./user');

const router = express.Router();

router.use('/other', otherRouter);
router.use('/auth', authRouter);
router.use('/user', userRouter);

module.exports = router;