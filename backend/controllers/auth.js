const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;
const bcrypt = require('bcrypt');
const User = require('../models').User;
const FacebookStrategy = require('passport-facebook').Strategy;

passport.use(new LocalStrategy(
    {
        usernameField: 'email',
        passwordField: 'password'
    },
    async (username, password, done) => {

        const user = await User.findOne({
            where: { email: username },
            raw: true
        }).catch(() => {
            return done(null, false, { message: "Database error." })
        });

        if (user) {
            bcrypt.compare(password, user.password, (err, result) => {
                if (result) {
                    return done(null, user);
                }
                else {
                    return done(null, false, { message: "Incorrect password." });
                }
            });
        }
        else {
            return done(null, false, { message: "Incorrect email address." })
        }
    }
));
passport.use(new FacebookStrategy(
    {
        clientID: '930020950812437',
        clientSecret: '0d0b310207ac5f882204102f5b9818f5'
        // callbackURL: '/facebookcallback'
    },
    function (accessToken, refreshToken, profile, cb) {
        // In this example, the user's Facebook profile is supplied as the user
        // record.  In a production-quality application, the Facebook profile should
        // be associated with a user record in the application's database, which
        // allows for account linking and authentication with other identity
        // providers.
        return cb(null, profile);
    }));
passport.serializeUser((user, done) => {
    done(null, user.id);
});

passport.deserializeUser(async (id, done) => {
    const user = User.findOne({ where: { id } });
    done(null, user);
});

const controller = {
    login: (req, res) => {
        res.status(200).send({ message: "Login was successful!" });
    },
    isAuthenticated: (req, res, next) => {
        if (!req.isAuthenticated()) {
            res.status(401).send({ message: "Unathorized." });
        }
        else {
            next();
        }
    },
    logout: (req, res) => {
        req.logout();
        res.status(200).send({ message: "Logout successful!" });
    },
    userData: async (req, res) => {
        const user = await req.user;
        res.send(user)
    }
};

module.exports = controller;