const Other = require('./other');
const Auth = require('./auth');
const User = require('./user');

const controller = {
    Other,
    Auth,
    User
};

module.exports = controller;